/* DMUTEX (2009) Sistemas Operativos Distribuidos
 * C�digo de Apoyo
 *
 * ESTE C�DIGO DEBE COMPLETARLO EL ALUMNO:
 *    - Para desarrollar las funciones de mensajes, reloj y
 *      gesti�n del bucle de tareas se recomienda la implementaci�n
 *      de las mismas en diferentes ficheros.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>

typedef struct proces
{
    char *id;
    struct sockaddr_in servaddr;
    struct proces *next;
} Proces;

enum
{
    MSG,
    LOCK,
    OK
};

Proces *createProces(char *id, int port);
int size(Proces *head);
void addProces(Proces **head, char *id, int port);
int getProcesPos(Proces *head, char *id);
Proces *searchById(Proces *head, char *id);
Proces *searchByPort(Proces *head, int port);
int *max(int *a, int *b, int n);
int isPrevius(int *a, int *b, int n);
Proces *pop(Proces **head);

int main(int argc, char *argv[])
{
    int port;
    char line[80], proc[80];

    int listenfd;
    struct sockaddr_in servaddr;
    int n_lock = 0;
    // int send_ok = 0;
    char *p_sec = NULL;
    int z_critica = 0;
    Proces *proces_list = NULL;
    Proces *send_ok_list = NULL;

    if (argc < 2)
    {
        fprintf(stderr, "Uso: proceso <ID>\n");
        return 1;
    }

    /* Establece el modo buffer de entrada/salida a l�nea */
    setvbuf(stdout, (char *)malloc(sizeof(char) * 80), _IOLBF, 80);
    setvbuf(stdin, (char *)malloc(sizeof(char) * 80), _IOLBF, 80);

    // Prueba de puertos libres.
    bzero(&servaddr, sizeof(servaddr));
    // Create a UDP Socket
    listenfd = socket(AF_INET, SOCK_DGRAM, 0);
    servaddr.sin_addr.s_addr = INADDR_ANY;
    servaddr.sin_port = 0;
    servaddr.sin_family = AF_INET;
    // bind server address to socket descriptor
    bind(listenfd, (struct sockaddr *)&servaddr, sizeof(servaddr));

    socklen_t len_inet = sizeof(servaddr);
    getsockname(listenfd, (struct sockaddr *)&servaddr, &len_inet);

    fprintf(stdout, "%s: %d\n", argv[1], servaddr.sin_port);

    for (; fgets(line, 80, stdin);)
    {
        if (!strcmp(line, "START\n"))
            break;

        int params = sscanf(line, "%[^:]: %d", proc, &port);
        /* Habra que guardarlo en algun sitio */

        if (params == 2)
        {
            addProces(&proces_list, proc, port);
        }
    }

    /* Inicializar Reloj */
    int *LC = (int *)malloc(sizeof(int) * size(proces_list));

    /* Procesar Acciones */

    for (; fgets(line, 80, stdin);)
    {
        if (!strcmp(line, "FINISH\n"))
        {
            break;
        }

        if (!strcmp(line, "EVENT\n"))
        {
            int pos = getProcesPos(proces_list, argv[1]);
            if (pos != -1)
            {
                LC[pos]++;
            }
            fprintf(stdout, "%s: %s\n", argv[1], "TICK");
            continue;
        }

        if (!strcmp(line, "GETCLOCK\n"))
        {
            fprintf(stdout, "%s: %s[%d", argv[1], "LC", LC[0]);
            int i;
            for (i = 1; i < size(proces_list); i++)
            {
                fprintf(stdout, ",%d", LC[i]);
            }
            fprintf(stdout, "%s\n", "]");
            continue;
        }

        if (!strcmp(line, "RECEIVE\n"))
        {
            // receive the datagram
            struct sockaddr_in cliaddr;
            socklen_t len = sizeof(cliaddr);

            char type;
            recvfrom(listenfd, &type, 1, MSG_PEEK, (struct sockaddr *)&cliaddr, &len);

            void *receive = malloc(1 + sizeof(int) * size(proces_list));
            recvfrom(listenfd, receive, 1 + sizeof(int) * size(proces_list), MSG_PEEK, (struct sockaddr *)&cliaddr, &len);
            int *LCr = (int *)malloc(sizeof(int) * size(proces_list));
            memcpy(LCr, receive + 1, sizeof(int) * size(proces_list));

            char *tipo;
            char *sec = NULL;
            if (type == MSG)
            {
                tipo = "MSG";
                // fprintf(stdout, "%s\n", "Terminando de recibir");
                recvfrom(listenfd, receive, 0, 0, (struct sockaddr *)&cliaddr, &len);
            }
            else if (type == LOCK)
            {
                tipo = "LOCK";

                void *rec_sec_len = malloc(1 + sizeof(int) * size(proces_list) + sizeof(size_t));
                recvfrom(listenfd, rec_sec_len, 1 + sizeof(int) * size(proces_list) + sizeof(size_t), MSG_PEEK, (struct sockaddr *)&cliaddr, &len);
                size_t *sec_len = (size_t *)malloc(sizeof(size_t));
                memcpy(sec_len, rec_sec_len + 1 + sizeof(int) * size(proces_list), sizeof(size_t));

                void *rec_sec = malloc(1 + sizeof(int) * size(proces_list) + sizeof(size_t) + *sec_len);
                recvfrom(listenfd, rec_sec, 1 + sizeof(int) * size(proces_list) + sizeof(size_t) + *sec_len, 0, (struct sockaddr *)&cliaddr, &len);
                sec = malloc(*sec_len);
                memcpy(sec, rec_sec + 1 + sizeof(int) * size(proces_list) + sizeof(size_t), *sec_len);

                Proces *temp = searchByPort(proces_list, cliaddr.sin_port);
                addProces(&send_ok_list, temp->id, temp->servaddr.sin_port);
            }
            else if (type == OK)
            {
                tipo = "OK";
                recvfrom(listenfd, receive, 0, 0, (struct sockaddr *)&cliaddr, &len);
            }
            // recvfrom(listenfd, LCr, sizeof(int) * size(proces_list), 0, (struct sockaddr *)&cliaddr, &len);
            if (p_sec != NULL && sec != NULL && !strcmp(p_sec, sec)) // Conflicto por la misma sec
            {
                int a_Prev_b = isPrevius(LC, LCr, size(proces_list));
                int b_Prev_a = isPrevius(LCr, LC, size(proces_list));

                if (a_Prev_b == b_Prev_a) // ninguno es anterior al otro
                {
                    // Zona conflictiva se elige el proceso que este antes en la lista
                    int pos_a = getProcesPos(proces_list, argv[1]);
                    Proces *b = searchByPort(proces_list, cliaddr.sin_port);
                    int pos_b = getProcesPos(proces_list, b->id);
                    if (pos_a < pos_b)
                    {
                        z_critica = 1;
                    }
                }
                else if (a_Prev_b > b_Prev_a)
                {
                    // a es anterior, hace LOCK
                    z_critica = 1;
                }
                else
                {
                    // b es anterior, Yo no hago lock
                    z_critica = 0;
                }
            }

            int *LCnew = max(LCr, LC, size(proces_list));

            LC = LCnew;

            Proces *origin = searchByPort(proces_list, cliaddr.sin_port);

            fprintf(stdout, "%s: RECEIVE(%s,%s)\n", argv[1], tipo, origin->id);

            int pos = getProcesPos(proces_list, argv[1]);
            if (pos != -1)
            {
                LC[pos]++;
            }

            fprintf(stdout, "%s: %s\n", argv[1], "TICK");

            if (type == LOCK && !z_critica)
            {
                int pos = getProcesPos(proces_list, argv[1]);
                if (pos != -1)
                {
                    LC[pos]++;
                }
                fprintf(stdout, "%s: %s\n", argv[1], "TICK");

                // 1 del tipo de mensaje y el resto del vector clock
                size_t tam = 1 + sizeof(int) * size(proces_list);
                // int type = MSG;
                void *buffer = malloc(tam);
                memset(buffer, OK, 1);
                memcpy(buffer + 1, LC, sizeof(int) * size(proces_list));

                sendto(listenfd, buffer, tam, 0, (const struct sockaddr *)&cliaddr, sizeof(cliaddr));

                Proces *origin = searchByPort(proces_list, cliaddr.sin_port);

                fprintf(stdout, "%s: SEND(OK,%s)\n", argv[1], origin->id);
                // send_ok--;
                pop(&send_ok_list);
            }
            else if (type == OK)
            {
                // fprintf(stdout, "procesos pendientes (%d)\n", n_lock);
                if (n_lock - 1 == 0)
                {
                    z_critica = 1;
                    fprintf(stdout, "%s: MUTEX(%s)\n", argv[1], p_sec);
                }
                n_lock--;
            }

            continue;
        }

        char action[80], id[80];
        sscanf(line, "%s %s", action, id);

        if (!strcmp(action, "MESSAGETO"))
        {
            int pos = getProcesPos(proces_list, argv[1]);
            if (pos != -1)
            {
                LC[pos]++;
            }
            fprintf(stdout, "%s: %s\n", argv[1], "TICK");

            Proces *dest = searchById(proces_list, id);

            // 1 del tipo de mensaje y el resto del vector clock
            size_t tam = 1 + sizeof(int) * size(proces_list);
            // int type = MSG;
            void *buffer = malloc(tam);
            memset(buffer, MSG, 1);
            memcpy(buffer + 1, LC, sizeof(int) * size(proces_list));

            sendto(listenfd, buffer, tam, 0, (const struct sockaddr *)&dest->servaddr, sizeof(dest->servaddr));

            fprintf(stdout, "%s: SEND(MSG,%s)\n", argv[1], id);

            continue;
        }

        if (!strcmp(action, "LOCK"))
        {
            p_sec = malloc(sizeof(strlen(id)));
            strcpy(p_sec, id);
            // z_critica = 1;

            int pos = getProcesPos(proces_list, argv[1]);
            if (pos != -1)
            {
                LC[pos]++;
            }
            fprintf(stdout, "%s: %s\n", argv[1], "TICK");

            // int broadcastPermission = 1;
            // setsockopt(listenfd, SOL_SOCKET, SO_BROADCAST, (void *)&broadcastPermission, sizeof(broadcastPermission));

            Proces *pointer = proces_list;
            while (pointer != NULL)
            {
                if (strcmp(pointer->id, argv[1]))
                {
                    size_t sec_len = strlen(p_sec);

                    // TIPO msg (1B) + VECTOR CLOCK (4B*N_PROCESOS) + SEC_LEN (4B) + SEC (1B*SEC_LEN)
                    size_t tam = 1 + sizeof(int) * size(proces_list) + sizeof(size_t) + sec_len;

                    void *buffer = malloc(tam);
                    memset(buffer, LOCK, 1);
                    memcpy(buffer + 1, LC, sizeof(int) * size(proces_list));
                    memcpy(buffer + 1 + sizeof(int) * size(proces_list), &sec_len, sizeof(size_t));
                    memcpy(buffer + 1 + sizeof(int) * size(proces_list) + sizeof(size_t), p_sec, sec_len);

                    sendto(listenfd, buffer, tam, 0, (const struct sockaddr *)&pointer->servaddr, sizeof(pointer->servaddr));

                    fprintf(stdout, "%s: SEND(LOCK,%s)\n", argv[1], pointer->id);
                    n_lock++;
                }
                pointer = pointer->next;
            }
            continue;
        }

        if (!strcmp(action, "UNLOCK"))
        {
            p_sec = NULL;
            z_critica = 0;
            if (size(send_ok_list) > 0)
            {
                int pos = getProcesPos(proces_list, argv[1]);
                if (pos != -1)
                {
                    LC[pos]++;
                }
                fprintf(stdout, "%s: %s\n", argv[1], "TICK");
                // fprintf(stdout, "%s (%d)\n", "me quedan por responder: ", size(send_ok_list));

                Proces *send_ok = pop(&send_ok_list);

                // 1 del tipo de mensaje y el resto del vector clock
                size_t tam = 1 + sizeof(int) * size(proces_list);

                void *buffer = malloc(tam);
                memset(buffer, OK, 1);
                memcpy(buffer + 1, LC, sizeof(int) * size(proces_list));

                sendto(listenfd, buffer, tam, 0, (const struct sockaddr *)&send_ok->servaddr, sizeof(send_ok->servaddr));

                fprintf(stdout, "%s: SEND(OK,%s)\n", argv[1], send_ok->id);
            }
            continue;
        }
    }

    // Proces *pointer = proces_list;
    // while (pointer != NULL)
    // {
    //     fprintf(stdout, "%s: %d, %d, %d, %s\n",
    //             pointer->id,
    //             pointer->servaddr.sin_addr.s_addr,
    //             pointer->servaddr.sin_family,
    //             pointer->servaddr.sin_port,
    //             pointer->servaddr.sin_zero);
    //     pointer = pointer->next;
    // }
    close(listenfd);
    return 0;
}

Proces *createProces(char *id, int port)
{
    Proces *new_proces = (Proces *)malloc(sizeof(Proces));

    struct sockaddr_in servaddr;

    // clear servaddr
    bzero(&new_proces->servaddr, sizeof(servaddr));
    new_proces->servaddr.sin_addr.s_addr = INADDR_ANY;
    new_proces->servaddr.sin_port = port;
    new_proces->servaddr.sin_family = AF_INET;

    new_proces->id = strdup(id);

    new_proces->next = NULL;
    return new_proces;
}

int size(Proces *head)
{
    int cont = 0;
    Proces *pointer = head;
    while (pointer != NULL)
    {
        cont++;
        pointer = pointer->next;
    }

    return cont;
}

void addProces(Proces **head, char *id, int port)
{
    if (size(*head) == 0)
    {
        *head = createProces(id, port);
        return;
    }
    Proces *pointer = *head;
    while (pointer->next != NULL)
    {
        pointer = pointer->next;
    }
    pointer->next = createProces(id, port);
}

Proces *pop(Proces **head)
{
    if (size(*head) > 0)
    {
        Proces *temp = *head;
        *head = temp->next;
        return temp;
    }
    return NULL;
}

int getProcesPos(Proces *head, char *id)
{
    int cont = 0;
    Proces *pointer = head;
    while (pointer != NULL)
    {
        if (!strcmp(id, pointer->id))
        {
            return cont;
        }
        pointer = pointer->next;
        cont++;
    }
    return -1;
}

Proces *searchById(Proces *head, char *id)
{
    Proces *pointer = head;
    while (pointer != NULL)
    {
        if (!strcmp(id, pointer->id))
        {
            return pointer;
        }
        pointer = pointer->next;
    }
    return NULL;
}

Proces *searchByPort(Proces *head, int port)
{
    Proces *pointer = head;
    while (pointer != NULL)
    {
        if (port == pointer->servaddr.sin_port)
        {
            return pointer;
        }
        pointer = pointer->next;
    }
    return NULL;
}

int *max(int *a, int *b, int n)
{
    int *aux = (int *)malloc(sizeof(int) * n);
    int i;
    for (i = 0; i < n; i++)
    {
        aux[i] = a[i] > b[i] ? a[i] : b[i];
    }
    return aux;
}

int isPrevius(int *a, int *b, int n)
{
    int i;
    for (i = 0; i < n; i++)
    {
        if (a[i] > b[i])
        {
            return 0;
        }
    }
    return 1;
}